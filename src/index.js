import React from 'react';
import ReactDOM from 'react-dom';
import './css/bootstrap.min.css';
import './css/font-awesome.min.css';
import AppConnect from './components/AppConnect'
import registerServiceWorker from './registerServiceWorker';
import thunk from 'redux-thunk';
import * as firebase from 'firebase';

import { ConnectedRouter } from 'react-router-redux'
import { Provider } from 'react-redux'

import { createStore, applyMiddleware } from 'redux'

import createBrowserHistory from 'history/createBrowserHistory'

import rootReducer from './reducers/index'

import sites from './data/sites'
import persons from './data/persons'
import eventLog from './data/eventLog'

firebase.initializeApp({
    apiKey: "AIzaSyCDMaUV36hovvaPe6KSA1qTVYrzAHSoB-4\n",
    authDomain: "buvnieki-2cf2f.firebaseapp.com",
    databaseURL: "https://buvnieki-2cf2f.firebaseio.com/",
    storageBucket: "bucket.appspot.com"
});


const defaultState = {
    sites,
    persons,
    eventLog,
    app: {addPerson: false}
};

const history = createBrowserHistory();
const store = createStore(rootReducer, defaultState, applyMiddleware(thunk));

ReactDOM.render(
    <Provider store={store}>
        <ConnectedRouter history={history}>
            <AppConnect/>
        </ConnectedRouter>
    </Provider>,
    document.getElementById('root'));
registerServiceWorker();
