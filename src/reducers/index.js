import {combineReducers} from 'redux'
import {routerReducer} from 'react-router-redux'

import sites from './sites';
import persons from './persons';
import eventLog from './eventLog';
import app from './app';

const rootReducer = combineReducers({sites, persons, eventLog, app, routing: routerReducer});

export default rootReducer
