function app(state = [], action)
{
    switch (action.type) {
        case 'SHOW_FORM':
            const showFormState = { ...state };

            if ( ! ('showForm' in showFormState)) {
                showFormState['showForm'] = {};
            }

            showFormState.showForm[action.name] = action.show;

            return showFormState;


        case 'HANDLE_ADD_DATA':
            const newState = { ...state };

            if ( ! ('addData' in newState)) {
                newState['addData'] = {};
            }

            if ( ! (action.context in newState.addData)) {
                newState.addData[action.context] = {};
            }

            newState.addData[action.context][action.fieldName] = action.val;

            return newState;

        default:
            return state;
    }
}

export default app;