function eventLog(state = [], action)
{
    switch (action.type) {
        case 'EVENTS_FETCH_DATA_SUCCESS':
            return action.events;

        case 'FETCH_EVENTS':
            console.log('Add new event.');
            return [
                ...state,
                {...state[0], type: 'stop'}
            ];

        case 'EVENT_LIST_UPDATED':
            const augmentedEvents = Object.keys(action.events).map((key) => {
                return {
                    ...action.events[key],
                    uid: key
                };
            });

            return  Object.values(augmentedEvents).reverse();

        default:
            return state;
    }
}

export default eventLog;