function sites(state = [], action)
{
    switch (action.type) {
        case 'SITE_LIST_UPDATED':
            return Object.values(action.sites);

        default:
            return state;
    }
}

export default sites;