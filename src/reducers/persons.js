function persons(state = [], action)
{
    switch (action.type) {
        case 'PERSON_LIST_UPDATED':
            return Object.values(action.persons);
        default:
            return state;
    }
}

export default persons;