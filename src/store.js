import {createStore, compose} from 'redux';
import {syncHistoryWithStore} from 'react-router-redux';
import {} from 'react-router';

import rootReducer from './reducers/index';

const defaultState = {};

const store = createStore(rootReducer, defaultState);

export default store;