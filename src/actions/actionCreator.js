export function personListUpdated(persons)
{
    return {
        type: 'PERSON_LIST_UPDATED',
        persons
    }
}

export function siteListUpdated(sites)
{
    return {
        type: 'SITE_LIST_UPDATED',
        sites
    }
}

export function eventListUpdated(events)
{
    return {
        type: 'EVENT_LIST_UPDATED',
        events
    }
}


export function showForm(data)
{
    return {
        type: 'SHOW_FORM',
        ...data
    }
}


export function handleAddData(path, event)
{
    return {
        type: 'HANDLE_ADD_DATA',
        context: path.context,
        fieldName: path.fieldName,
        val: event.target.value
    }
}



// This one is just with purpose of reference.
export function fetchRemoteEvents()
{
    return (dispatch) => {
        console.log('From within callback.');
        dispatch(() => { return { type: 'FETCH_EVENTS' } });
    }
}