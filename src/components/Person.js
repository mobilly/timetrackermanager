import React, {Component} from 'react';
import {Link} from 'react-router-dom';

class Person extends Component
{
    render ()
    {
        const { firstName, lastName, personCode, uid } = this.props.person;

        return (
            <tr>
                <td>{ firstName }</td>
                <td>{ lastName }</td>
                <td>{ personCode }</td>
                <th>
                    <Link to={`/person/${uid}`}><i className="fa fa-pencil"></i></Link>
                </th>
            </tr>
        )
    }
}

export default Person;