import React, {Component} from 'react';
import Person from './Person';
import * as firebase from 'firebase';

class Persons extends  Component
{
    render()
    {
        let addPersonForm = null;
        if ('showForm' in this.props.app && 'addPerson' in this.props.app.showForm && this.props.app.showForm.addPerson) {
            addPersonForm = (
                <tr>
                    <td>
                        <div className="form-group">
                            <input type="text" onChange={this.props.handleAddData.bind(null, {fieldName: 'firstName', context: 'person'})} className="form-control" id="addPersonFirstName" placeholder="Vārds" />
                        </div>
                    </td>
                    <td>
                        <div className="form-group">
                            <input type="text" onChange={this.props.handleAddData.bind(null, {fieldName: 'lastName', context: 'person'})} className="form-control" id="addPersonLastName" placeholder="Uzvārds" />
                        </div>
                    </td>
                    <td><div className="form-group">
                        <input type="text" onChange={this.props.handleAddData.bind(null, {fieldName: 'personCode', context: 'person'})} className="form-control" id="addPersonPersonCode" placeholder="Personas kods" />
                    </div></td>
                    <th>
                        <button onClick={this.savePerson.bind(this)} className="btn btn-primary">Labi</button>
                    </th>
                </tr>
            );
        }

        return (
            <div className="container">
                <div className="row">
                    <div className="col">
                        <h1>Personas</h1>
                        <p>
                            <button onClick={this.props.showForm.bind(null, {name: 'addPerson', show: true})} className="btn btn-primary">Pievienot</button>
                        </p>
                        <table className="table">
                            <thead className="thead-default">
                                <tr>
                                    <th>Vārds</th>
                                    <th>Uzvārds</th>
                                    <th>Personas kods</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                            { this.props.persons.map((person, i) => <Person {...this.props} key={i} i={i} person={person} />) }
                            { addPersonForm }
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        );
    }


    savePerson()
    {
        const personRef = firebase.database().ref('person').push();
        personRef.set({
            ...this.props.app.addData.person,
            uid: personRef.key
        });
        this.props.showForm({name: 'addPerson', show: false});
    }
}

export default Persons;