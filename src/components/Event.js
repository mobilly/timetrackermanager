import React, {Component} from 'react';
import moment from 'moment';


class Event extends Component
{
    render ()
    {
        const { time, type, uid } = this.props.event;
        const typeClass = type === 'stop' ? 'fa fa-stop text-muted' : 'fa fa-play text-muted';
        const eventTime = moment(parseInt(time, 10));

        return (
            <tr>
                <td><i className={typeClass} /></td>
                <td><abbr title={ uid }>{ eventTime.format('DD.MM.YYYY HH:mm') }</abbr></td>
                <td>{ type }</td>
                <td>{ this.personName() }</td>
                <td>{ this.siteName() }</td>
            </tr>
        )
    }

    personName()
    {
        if ('person' in this.props.event) {
            const thePerson = this.props.event.person;
            return thePerson.firstName + ' ' + thePerson.lastName;
        }

        return this.props.event.personId;
    }

    siteName()
    {
        if ('site' in this.props.event) {
            const theSite = this.props.event.site;
            return theSite.name + ' (' + theSite.address + ')';
        }

        return this.props.event.siteId;
    }
}

export default Event;