import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as actionCreators from '../actions/actionCreator'
import App from './App'

function mapStateToProps(state, ownProps)
{
    return {
        sites: state.sites,
        persons: state.persons,
        eventLog: state.eventLog,
        app: state.app,
        routing: state.routing
    }
}

function mapDispatchToProps(dispatch)
{
    return bindActionCreators(actionCreators, dispatch);
}

const AppConnect = connect(mapStateToProps, mapDispatchToProps)(App);

export default AppConnect;