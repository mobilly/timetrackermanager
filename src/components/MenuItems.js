import React, {Component} from 'react';
import {Link} from 'react-router-dom';

class MenuItems extends Component
{
    render()
    {
        return (
            <div className="collapse navbar-collapse" id="navbarNav">
                <ul className="navbar-nav">
                    <li className="nav-item">
                        <Link to="/events" className="nav-link">Notikumi</Link>
                    </li>
                    <li className="nav-item">
                        <Link to="/sites" className="nav-link">Objekti</Link>
                    </li>
                    <li className="nav-item">
                        <Link to="/persons" className="nav-link">Personas</Link>
                    </li>
                </ul>
            </div>
        );
    }
}

export default MenuItems;