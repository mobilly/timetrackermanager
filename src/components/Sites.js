import React, {Component} from 'react';
import Site from './Site';
import * as firebase from 'firebase';

class Sites extends Component
{
    render()
    {
        let addItemForm = null;
        if ('showForm' in this.props.app && 'addSite' in this.props.app.showForm && this.props.app.showForm.addSite) {
            addItemForm = (
                <tr>
                    <td>
                        <div className="form-group">
                            <input type="text" onChange={this.props.handleAddData.bind(null, {fieldName: 'name', context: 'site'})} className="form-control" id="addPersonFirstName" placeholder="Nosaukums" />
                        </div>
                    </td>
                    <td>
                        <div className="form-group">
                            <input type="text" onChange={this.props.handleAddData.bind(null, {fieldName: 'address', context: 'site'})} className="form-control" id="addPersonLastName" placeholder="Adrese" />
                        </div>
                    </td>
                    <td><div className="form-group">
                        <input type="text" onChange={this.props.handleAddData.bind(null, {fieldName: 'description', context: 'site'})} className="form-control" id="addPersonPersonCode" placeholder="Apraksts" />
                    </div></td>
                    <th>
                        <button onClick={this.saveSite.bind(this)} className="btn btn-primary">Labi</button>
                    </th>
                </tr>
            );
        }

        return (
            <div className="container">
                <div className="row">
                    <div className="col">
                        <h1>Objekti</h1>
                        <p>
                            <button onClick={this.props.showForm.bind(null, {name: 'addSite', show: true})} className="btn btn-primary">Pievienot</button>
                        </p>
                        <table className="table">
                            <thead className="thead-default">
                                <tr>
                                    <th>Nosaukums</th>
                                    <th>Adrese</th>
                                    <th>Apraksts</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                            { this.props.sites.map((site, i) => <Site {...this.props} key={i} i={i} site={site} />) }
                            { addItemForm }
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        );
    }

    saveSite()
    {
        const personRef = firebase.database().ref('sites').push();
        personRef.set({
            ...this.props.app.addData.site,
            uid: personRef.key
        });
        this.props.showForm({name: 'addSite', show: false});
    }
}

export default Sites;