import React, {Component} from 'react';
import Event from './Event';

class Events extends Component
{
    render()
    {
        return (
            <div className="container">
                <div className="row">
                    <div className="col">
                        <h1>Notikumi</h1>
                        <table className="table">
                            <thead className="thead-default">
                                <tr>
                                    <th>&nbsp;</th>
                                    <th>Laiks</th>
                                    <th>Notikuma veids</th>
                                    <th>Persona</th>
                                    <th>Būvobjekts</th>
                                </tr>
                            </thead>
                            <tbody>
                            { this.props.eventLog.map((event, i) => <Event {...this.props} key={i} i={i} event={event} />) }
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        );
    }
}

export default Events;