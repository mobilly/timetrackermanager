import React, {Component} from 'react';
import {Link} from 'react-router-dom';

class Site extends Component
{
    render ()
    {
        const { name, address, description, uid } = this.props.site;

        return (
            <tr>
                <td>{ name }</td>
                <td>{ address }</td>
                <td>{ description }</td>
                <td>
                    <Link to={`/site/${uid}`}><i className="fa fa-pencil"></i></Link>
                </td>
            </tr>
        )
    }
}

export default Site;