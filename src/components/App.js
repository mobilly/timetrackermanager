import React, { Component } from 'react'
import { Route } from 'react-router-dom'
import moment from 'moment';

import '../App.css';

import * as firebase from 'firebase';

import Menu from "./Menu"
import Sites from "./Sites"
import SiteEdit from "./SiteEdit"
import Persons from './Persons'
import PersonEdit from './PersonEdit'
import Events from './Events'
import Login from './Login'


class App extends Component
{
    componentDidMount()
    {
        this.createEvent();

        const _this = this;

        firebase.database().ref('event')
            .limitToLast(10)
            .on('value', function(snapshot) {
                _this.props.eventListUpdated(snapshot.val());
            });

        firebase.database().ref('person')
            .on('value', function(snapshot) {
                _this.props.personListUpdated(snapshot.val());
            });

        firebase.database().ref('sites')
            .on('value', function(snapshot) {
                _this.props.siteListUpdated(snapshot.val());
            });

    }


    render() {
        return (
            <div>
                <Menu {...this.props} />

                <p>&nbsp;</p>

                <Route exact path="/events" render={ (props) => <Events {...this.props} /> }/>
                <Route exact path="/sites" render={ (props) => <Sites {...this.props} /> }/>
                <Route exact path="/persons" render={ (props) => <Persons {...this.props} /> }/>
                <Route exact path="/person/:id" component={PersonEdit}/>
                <Route exact path="/site/:id" component={SiteEdit}/>
                <Route exact path="/" component={ Login }/>
            </div>
        )
    }

    createEvent()
    {
        const personPromise = firebase.database().ref('person/0').once('value');
        const sitePromise = firebase.database().ref('sites/1').once('value');
        Promise.all([personPromise, sitePromise])
            .then((values) => {
                const newEventRef = firebase.database().ref('event').push();
                newEventRef.set({
                    uid: newEventRef.key,
                    person: values[0].val(),
                    relatedId: "",
                    site: values[1].val(),
                    time: moment().format('x'),
                    type: Math.random() < 0.5 ? "start" : "stop"
                });

            });
    }
}

export default App;
