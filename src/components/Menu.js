import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import MenuItems from './MenuItems';

class Menu extends Component
{
    render()
    {
        const menuItems = this.props.routing.location.pathname === '/' ? null : <MenuItems/>;

        return (
            <nav className="navbar navbar-expand-lg navbar-light bg-light">

                <Link to="/" className="navbar-brand"><i className="fa fa-th"></i></Link>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                { menuItems }
            </nav>
        );
    }
}

export default Menu;